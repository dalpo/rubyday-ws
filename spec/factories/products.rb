FactoryGirl.define do
  factory :product do
    image "MyString"
    name "MyString"
    abstract "MyString"
    description "MyText"
  end
end
