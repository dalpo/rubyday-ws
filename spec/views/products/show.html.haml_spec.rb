require 'rails_helper'

RSpec.describe "products/show", type: :view do
  before(:each) do
    @product = assign(:product, Product.create!(
      :image => "Image",
      :name => "Name",
      :abstract => "Abstract",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Image/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Abstract/)
    expect(rendered).to match(/MyText/)
  end
end
