require 'rails_helper'

RSpec.describe "products/edit", type: :view do
  before(:each) do
    @product = assign(:product, Product.create!(
      :image => "MyString",
      :name => "MyString",
      :abstract => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit product form" do
    render

    assert_select "form[action=?][method=?]", product_path(@product), "post" do

      assert_select "input#product_image[name=?]", "product[image]"

      assert_select "input#product_name[name=?]", "product[name]"

      assert_select "input#product_abstract[name=?]", "product[abstract]"

      assert_select "textarea#product_description[name=?]", "product[description]"
    end
  end
end
