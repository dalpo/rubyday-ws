require 'rails_helper'

RSpec.describe "products/new", type: :view do
  before(:each) do
    assign(:product, Product.new(
      :image => "MyString",
      :name => "MyString",
      :abstract => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new product form" do
    render

    assert_select "form[action=?][method=?]", products_path, "post" do

      assert_select "input#product_image[name=?]", "product[image]"

      assert_select "input#product_name[name=?]", "product[name]"

      assert_select "input#product_abstract[name=?]", "product[abstract]"

      assert_select "textarea#product_description[name=?]", "product[description]"
    end
  end
end
