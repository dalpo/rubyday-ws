class Product < ApplicationRecord
  mount_uploader :image

  validates :name, presence: true
end
