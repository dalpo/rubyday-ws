json.extract! product, :id, :image, :name, :abstract, :description, :created_at, :updated_at
json.url product_url(product, format: :json)