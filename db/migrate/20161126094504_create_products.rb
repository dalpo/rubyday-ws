class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :image
      t.string :name
      t.string :abstract
      t.text :description

      t.timestamps
    end
  end
end
